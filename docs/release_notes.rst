Release Notes
*************

*Noviembre 10, 2020*

Detalles de Release 1.11.6
=============================


Fixes y mejoras
--------------------------
- Se solucionan errores de dialplan relacionados con logs de transferencia de llamadas
- La vista de supervisión de agentes fue optimizada para mejorar su performance
